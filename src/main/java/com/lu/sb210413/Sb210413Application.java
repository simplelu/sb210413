package com.lu.sb210413;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sb210413Application {

    public static void main(String[] args) {
        SpringApplication.run(Sb210413Application.class, args);
    }

}
