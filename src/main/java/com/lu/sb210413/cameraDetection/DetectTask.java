package com.lu.sb210413.cameraDetection;

import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetSocketAddress;

@Component
@EnableScheduling
public class DetectTask {


    @Scheduled(cron = "${rtsp.detect-time}")
    public void hello() {

        RTSPClient rtspClient = new RTSPClient(
                new InetSocketAddress("192.168.4.64", 554),
                "rtsp://192.168.4.64/h264/cn1/main/av_stream/", "admin", "12345asdf");
        rtspClient.start();

        RTMPClient rtmpClient = new RTMPClient("http://1.14.13.60:10101/live/fire.flv");
        rtmpClient.start();
    }

    // https://blog.csdn.net/user_xiangpeng/article/details/78970189
    public static void main(String[] args) throws IOException {

        File file = new File("C:\\Users\\Administrator\\Desktop\\test.jpg");
        ByteArrayInputStream intputStream = new ByteArrayInputStream(FileUtils.readFileToByteArray(file));
        Thumbnails.Builder<? extends InputStream> builder = Thumbnails.of(intputStream).scale(0.3);
        try {
            BufferedImage bufferedImage = builder.asBufferedImage();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, "jpg", baos);
            byte[] byteArray = baos.toByteArray();
            System.out.println(Base64Utils.encodeToString(byteArray));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
