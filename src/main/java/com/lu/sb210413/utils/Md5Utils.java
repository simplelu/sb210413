package com.lu.sb210413.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Objects;

/**
 * Md5加密方法
 *
 * @author Crown
 */
public class Md5Utils {

    private static final Logger log = LoggerFactory.getLogger(Md5Utils.class);

    private static byte[] md5(byte[] bytes) {
        MessageDigest algorithm;
        try {
            algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(bytes);
            return algorithm.digest();
        } catch (Exception e) {
            log.error("MD5 Error...", e);
        }
        return null;
    }

    private static String toHex(byte[] hash) {
        if (hash == null) {
            return null;
        }
        StringBuilder buf = new StringBuilder(hash.length * 2);
        int i;

        for (i = 0; i < hash.length; i++) {
            if ((hash[i] & 0xff) < 0x10) {
                buf.append("0");
            }
            buf.append(Long.toString(hash[i] & 0xff, 16));
        }
        return buf.toString();
    }

    public static String hash(byte[] bytes) {
        try {
            return new String(Objects.requireNonNull(toHex(md5(bytes))).getBytes(StandardCharsets.UTF_8),
                    StandardCharsets.UTF_8);
        } catch (Exception e) {
            log.error("not supported charset... {}", e.getMessage());
            return "";
        }
    }

    public static String hash(String s) {
        try {
            return hash(s.getBytes(StandardCharsets.UTF_8));
        } catch (Exception e) {
            log.error("not supported charset... {}", e.getMessage());
            return s;
        }
    }
}
